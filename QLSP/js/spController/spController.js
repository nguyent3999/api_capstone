export let layThongTinTuForm = () => {
  const id = document.getElementById("IdSP").value
  const name = document.getElementById("TenSP").value;
  const price = document.getElementById("GiaSP").value;
  const screen = document.getElementById("screen").value;
  const backCamera = document.getElementById("backCamera").value;
  const frontCamera = document.getElementById("frontCamera").value;
  const img = document.getElementById("HinhSP").value;
  const desc = document.getElementById("MoTa").value;
  const type = document.getElementById("loai").value;

  return {
    id: id,
    name: name,
    price: price,
    screen: screen,
    backCamera: backCamera,
    frontCamera: frontCamera,
    img: img,
    desc: desc,
    type: type
  };
};
export let renderDsSp = (list) => {
  let contentHTML = "";

  list.forEach((sp) => {
    contentHTML += `<tr>
      <td>${sp.id}</td>
      <td>${sp.name}</td>
      <td>${sp.price}</td>
      <td>${sp.screen}</td>
      <td>${sp.backCamera}</td>
      <td>${sp.frontCamera}</td>
      <td>${sp.img}</td>
      <td>${sp.desc}</td>
      <td>${sp.type}</td>
      <td>
        <button onclick="delSp('${sp.id}')" class="btn btn-danger">Xoá</button>
        <button onclick="editSpSrv('${sp.id}')" data-toggle="modal" data-target="#myModal" class="btn btn-success">Sửa</button>
      </td>
    </tr>`
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

export let showThongTinLenForm = (data) => {
  document.getElementById("IdSP").value = data.id;
  document.getElementById("TenSP").value = data.name;
  document.getElementById("GiaSP").value = data.price;
  document.getElementById("screen").value = data.screen;
  document.getElementById("backCamera").value = data.backCamera;
  document.getElementById("frontCamera").value = data.frontCamera;
  document.getElementById("HinhSP").value = data.img;
  document.getElementById("MoTa").value = data.desc;
  document.getElementById("loai").value = data.type;
}