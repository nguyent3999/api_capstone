// Ham render so luong san pham cua modal Cart
// ----start-----
export let renderQuantityCartItemModal = (value, itemName) => {
  // document.querySelector(`.${itemName}`).setAttribute("value", value);
  document.querySelector(`.${itemName}`).value = value;
};
//---end---

// Ham render notify cua icon gio hang
// ------ start -------
export const renderNotify = (value) => {
  document.querySelector("#show-quantity").setAttribute("data-notify", value);
};
// ----end-=
// export let renderQuantityCartModal = (value) => {
//   document.querySelector(`#$`).setAttribute("value", value);
//   document.querySelector("#product-quantity").value = value;
// };

const createDiv = ({ img, name, price }, quantity) => {
  let nameClass = name.replace(/ +/g, "-"); // Xoa het khoang trang va tao class co ten trung voi nameClass de truy xuat
  return ` <li class="header-cart-item flex-t m-b-12">
  <div onclick='deleteCartItem("${name}", "${quantity}")'><i role="button" class="text-danger fa fa-trash-alt"></i></div>
  <div class="header-cart-item-img">
    <img src=${img} alt="IMG" />
  </div>

  <div class="header-cart-item-txt p-t-8">
    <a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
      ${name}
    </a>

    <span class="header-cart-item-info"> ${quantity} x $${price} </span>
  </div>
  
   <div class="wrap-num-product flex-w m-tb-10">
    <div onclick='minusCart("${name}")'  class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
      <i class="fs-16 zmdi zmdi-minus"></i>
    </div>

    <input class="cart-quantity ${nameClass} mtext-104 cl3 txt-center num-product" value="${quantity}" />


    <div onclick='plusCart("${name}")' class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
      <i class="fs-16 zmdi zmdi-plus"></i>
    </div>

</li>`;
};
export const renderCart = (list) => {
  let contentCart = "";
  let total = 0;
  list.forEach((item) => {
    contentCart += createDiv(item.product, item.quantity);
    total += item.product.price * 1 * item.quantity;
  });
  document.querySelector("#cart-list").innerHTML = contentCart;
  document.querySelector("#total").setAttribute("value", total);
  document.querySelector("#total").textContent = `Total: ${total}$`;
};

export const renderTotal = (value) => {
  // let total = document.querySelector("#total").
  document.querySelector("#total").textContent = `Total: ${value}$`;
  document.querySelector("#total").setAttribute("value", value);
};

// Ham tinh tong tong so luong san pham trong cart
// ----start----
export const calcQuantity = (list) => {
  let total = 0;
  if (!list) list = []; // neu list rong thi list = []
  list.forEach((item) => {
    total += item.quantity;
  });
  return total;
};
//---end---
// Ham tinh tong tong price cua cac san pham trong cart
// ----start----
export const calcTotal = (list) => {
  let total = 0;
  list.forEach((item) => {
    total += item.product.price * item.quantity;
  });
  return total;
};
//-----end-------
