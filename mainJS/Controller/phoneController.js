let createDiv = (hinhAnh, tenSp, giaTien, moTa, cameraTruoc, cameraSau) => {
  return `<div class=" col-lg-3 p-b-35 isotope-item women">
  <!-- Block2 -->
  <div class="block2">
    <div class="block2-pic hov-img0">
      <img src=${hinhAnh} alt="IMG-PRODUCT" />

      <a
        onclick= "quickView('${hinhAnh}','${tenSp}','${giaTien}','${moTa}','${cameraTruoc}','${cameraSau}')"
        class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1"
      >
        Quick View
      </a>
    </div>

    <div class="block2-txt flex-w flex-t p-t-14">
      <div class="block2-txt-child1 flex-col-l">
        <a
          href="product-detail"
          class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6"
        >
          ${tenSp}
        </a>

        <span class="stext-105 cl3"> ${giaTien}$ </span>
      </div>

      <div class="block2-txt-child2 flex-r p-t-3">
        <a
          href="#"
          class="btn-addwish-b2 dis-block pos-relative js-addwish-b2"
        >
          <img
            class="icon-heart1 dis-block trans-04"
            src="images/icons/icon-heart-01.png"
            alt="ICON"
          />
          <img
            class="icon-heart2 dis-block trans-04 ab-t-l"
            src="images/icons/icon-heart-02.png"
            alt="ICON"
          />
        </a>
      </div>
    </div>
  </div>
</div>
`;
};

export const renderDienThoai = (list) => {
  let contentHTML = '';
  list.forEach((item) => {
    contentHTML += createDiv(
      item.img,
      item.name,
      item.price,
      item.desc,
      item.frontCamera,
      item.backCamera
    );
  });
  document.querySelector('.isotope-grid').innerHTML = contentHTML;
};

// ham render thong tin modal quickView
export const changeInnertext = (...info) => {
  document.querySelector('#imgModal').src = info[0];
  document.querySelector('#phone-name').innerText = info[1];
  document.querySelector('#phone-price').innerText = `${info[2]}$`;
  document.querySelector('#phone-desc').innerText = info[3];
  document.querySelector('#front-camera').innerText = info[4];
  document.querySelector('#back-camera').innerText = info[5];
};

// Ham render so luong san pham cua modal quickView
// ----start-----
export let renderQuantity = (value) => {
  if (value < 0) value = 0;
  // document.querySelector("#product-quantity").setAttribute("value", value);
  document.querySelector('#product-quantity').value = value;
};
// ---end---
