import { CartItem } from '../Model/cartItemModel.js';
import { Phone } from '../Model/phoneModel.js';
import { getListDienThoai } from '../Service/dienThoaiService.js';
import {
  calcQuantity,
  calcTotal,
  renderCart,
  renderNotify,
  renderQuantityCartItemModal,
  renderTotal,
} from './cartController.js';
// import { addToCart } from "./cartController.js";
import {
  changeInnertext,
  renderDienThoai,
  renderQuantity,
} from './phoneController.js';

let productList = [];
let samsungList = [];
let iphoneList = [];
let cart = [];
let cartQuantity = 0;

// axios
getListDienThoai()
  .then((res) => {
    console.log(res.data);
    productList = res.data.map((item) => {
      //   for (let key in item) {
      //     console.log(item[key]);
      //   }
      item.desc = item.desc.replaceAll('"', ''); // Loc lai thong tin tu Back-end
      for (let index in item) item[index] = item[index].trim(); //Xoa het khoang trang
      return new Phone(
        item.name,
        item.price,
        item.screen,
        item.backCamera,
        item.frontCamera,
        item.img,
        item.desc,
        item.type
      );
    });
    renderDienThoai(productList);
    productList.forEach((phone) => {
      phone.type.toLowerCase() == 'samsung'
        ? samsungList.push(phone)
        : iphoneList.push(phone);
    });
    console.log(productList);
  })
  .catch((err) => {
    console.log(err);
  });

// Render lai cart moi khi load trang
// Lay data tu local storage
let dataJson = localStorage.getItem('Cart');
if (dataJson) {
  let dataRaw = JSON.parse(dataJson);
  cart = dataRaw.map(function (item) {
    return new CartItem(item.product, item.quantity);
  });
  renderCart(cart);
  renderNotify(calcQuantity(cart));
}
const saveCartToLocalStorage = () => {
  let CartJson = JSON.stringify(cart);
  localStorage.setItem('Cart', CartJson);
};
//

// Filter theo loai
let filterTheoLoai = () => {
  let type = document.querySelector('#sortByType').value;
  type == 'samsung'
    ? renderDienThoai(samsungList)
    : type == 'iphone'
    ? renderDienThoai(iphoneList)
    : renderDienThoai(productList);
};
window.filterTheoLoai = filterTheoLoai;

// modal khi click quickView
const quickView = (hinhAnhUrl, tenSp, giaSp, moTa, cameraTruoc, cameraSau) => {
  // Gia tri default cua quantity
  let itemQuantity = 1;
  renderQuantity(itemQuantity);

  // Bat modal
  document.querySelector('#modal').classList.add('show-modal1');
  //Them thong tin cua item hien tai vao modal
  changeInnertext(hinhAnhUrl, tenSp, giaSp, moTa, cameraTruoc, cameraSau);
  // CHANGE QUANTITY
  let plusItem = document.querySelector('#plus-quantity');
  let minusItem = document.querySelector('#minus-quantity');

  //Them onclick vao nut '+' va '-'
  plusItem.onclick = function () {
    itemQuantity++;
    renderQuantity(itemQuantity);
  };
  minusItem.onclick = function () {
    itemQuantity--;
    if (itemQuantity < 0) itemQuantity = 0;
    renderQuantity(itemQuantity);
  };

  // ADD TO CART BUTTON
  let addCartBtn = document.querySelector('#addToCart');
  addCartBtn.onclick = function () {
    cartQuantity += itemQuantity;
    console.log('cartQuantity: ', cartQuantity);
    console.log('calcQuantity(cart): ', calcQuantity(cart));
    renderNotify(calcQuantity(cart) + itemQuantity);

    addToCart(tenSp, itemQuantity);
    renderCart(cart);
    saveCartToLocalStorage();
  };
};
window.quickView = quickView;

// Ham add san pham vao cart dong thoi tao object lop CartItem
const addToCart = (productName, productQuantity) => {
  let indexCart = cart.findIndex((cartItem) => {
    // console.log(price);
    return productName == cartItem.product.name;
  });
  if (indexCart == -1) {
    // Neu khong tim thay ten cua san pham thi tao moi Object
    let indexProduct = productList.findIndex((productItem) => {
      return productName == productItem.name;
    });
    if (indexProduct == -1) return;
    // addToCart(indexProduct);
    let newItem = {
      product: { ...productList[indexProduct] },
      quantity: productQuantity,
    };
    let cartItem = new CartItem(newItem.product, newItem.quantity);
    cart.push(cartItem);
  } else {
    cart[indexCart].quantity += productQuantity;
  }
};

// Ham xoa san pham
const deleteCartItem = (tenSp, quantity) => {
  let index = cart.findIndex((item) => {
    return item.product.name == tenSp;
  });
  if (index == -1) return;
  console.log(cartQuantity);
  let currentQuantity = document
    .querySelector('#show-quantity')
    .getAttribute('data-notify');
  cartQuantity = currentQuantity * 1 - quantity;
  cart.splice(index, 1);
  renderNotify(calcQuantity(cart));
  saveCartToLocalStorage();
  renderCart(cart);
};
window.deleteCartItem = deleteCartItem;

const minusCart = (value) => {
  let index = cart.findIndex((item) => {
    return item.product.name == value;
  });
  cart[index].quantity--;
  if (cart[index].quantity < 0) cart[index].quantity = 0;
  let cartItemName = cart[index].product.name.replace(/ +/g, '-'); // truyen vao class name (bo khoang trang va them '-' giua cac chu cai)
  renderQuantityCartItemModal(cart[index].quantity, cartItemName);
  renderTotal(calcTotal(cart));
  renderNotify(calcQuantity(cart));
  saveCartToLocalStorage();
};
window.minusCart = minusCart;

const plusCart = (value) => {
  let index = cart.findIndex((item) => {
    return item.product.name == value;
  });
  console.log(cart[index].quantity);
  cart[index].quantity++;
  let cartItemName = cart[index].product.name.replace(/ +/g, '-'); // truyen vao class name (bo khoang trang va them '-' giua cac chu cai)
  renderQuantityCartItemModal(cart[index].quantity, cartItemName);
  renderTotal(calcTotal(cart));
  renderNotify(calcQuantity(cart));
  saveCartToLocalStorage();
};
window.plusCart = plusCart;

// them click vao nut checkout
document.querySelector('#checkout').addEventListener('click', () => {
  cart = [];
  saveCartToLocalStorage();
  window.location.reload();
});
